const Query = {
    me: () => ({
        id: '1234aBcD',
        name: 'Arunav',
        email: 'test@test.com',
        age: 24
    }),
    post: () => ({
        id: '1234SDcd',
        title: 'First Post on GraphQL',
        body: '',                        // not null
        published: false
    }),
    users: (parent, args, { db }, info) => {
        if(!args.query)
            return db.users

        return db.users.filter(user => {
           return user.name.toLowerCase().includes(args.query.toLowerCase())
        })
    },
    posts: (parent, args, { db }, info) => {
        if(!args.query)
            return db.posts
        
        return db.posts.filter(post => {
            return (
                post.body.toLowerCase().includes(args.query.toLowerCase()) ||
                post.title.toLowerCase().includes(args.query.toLowerCase())
            )
        })
    },
    comments: (parent, args, { db }, info) => {
        if(!args.query)
            return db.comments
        
        return db.comments.filter(comment => comment.text.toLowerCase().includes(args.query.toLowerCase()))
    }
}

export default Query