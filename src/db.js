const users = [
    {
        id: '1',
        name: 'Arunav',
        email: 'arunav@tezt.com',
        age: 24
    },
    {
        id: '2',
        name: 'Harsh',
        email: 'harsh@tezt.com'
    },
    {
        id: '3',
        name: 'Jethalal',
        email: 'jethiya@tezt.com'
    }
]

const posts = [
    {
        id: 'p123',
        title: 'First title of gql',
        body: 'This is the first body of gql',
        published: true,
        author: '1'
    },
    {
        id: 'p345',
        title: 'Liggi',
        body: 'This is the second body of gql',
        published: true,
        author: '1'
    },
    {
        id: 'p678',
        title: 'Udd Gaye',
        body: '',
        published: false,
        author: '2'
    }
]

const comments = [
    {
        id: 'c1',
        text: 'This is my comment',
        author: '1',
        postId: 'p123'
    },
    {
        id: 'c2',
        text: 'This is a graphql course',
        author: '2',
        postId: 'p678'
    },
    {
        id: 'c3',
        text: 'Third comment in this list',
        author: '3',
        postId: 'p123'
    },
    {
        id: 'c4',
        text: 'Graphql is really good alternative to REST API',
        author: '1',
        postId: 'p123'
    }
]

const db = {
    users,
    posts,
    comments
}

export default db