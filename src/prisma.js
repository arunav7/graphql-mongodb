import { Prisma } from 'prisma-binding'

const prisma = new Prisma({
    typeDefs: 'src/generated/prisma.graphql',
    endpoint: 'http://192.168.99.100:4466'
})

const createPostForUser = async (authorId, data) => {
    try {
        const userExists = await prisma.exists.User({ id: authorId })
        if(!userExists)
            throw new Error('User not found')

        const post = await prisma.mutation.createPost({
                data: {
                    ...data,
                    author: {
                        connect: {
                            id: authorId                    
                        }
                    }
                }
            }, `{ author { id name email posts { id title body published } } }`) 

        return post.author
    }catch(error) {
        const err = new Error('Not a valid Id')
        console.log(err)
    }    
}

const updatePostForUser = async (postId, data) => {
    try {
        const postExists = await prisma.exists.Post({ id: postId })
        if(!postExists)
            throw new Error('Post not found')

        const post = await prisma.mutation.updatePost({
            where: {
                id: postId
            },
            data
        }, `{ author { id name email posts { id title body published } } }`)

        return post.author
    }catch(error) {
        const err = new Error('Not a valid Id')
        console.log(err.message)
    }
}

// createPostForUser('5f210680a7b11b00071f5fcb', {
//     title: 'Hello World',
//     body: '',
//     published: false
// }).then(user => {
//     console.log(JSON.stringify(user, undefined, 2))
// }).catch(err => console.log(err.message))


updatePostForUser('5f213fffa7b11b0007648f3d',{
    body: 'this is the hello world body. just dummy random texts',
    published: true
}).then(user => {
    console.log(JSON.stringify(user, undefined, 2))
}).catch(err => console.log(err.message))
